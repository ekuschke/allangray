package allangray.fileParsing;

import allangray.collections.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class UserFileLineParserTest {
    
    public UserFileLineParserTest() {
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void InCorrect_InputLine_throws_IllegalArgumentException() throws Exception {
        String inputLine = "John likes to be following Jane";
        UserFileLineParser.Parse(inputLine);
    }
    
    @Test
    public void Parse_Correct_InputLine_one_follower() throws Exception {
        String inputLine = "John follows Jane";
        Digraph<String> graph = UserFileLineParser.Parse(inputLine);
        
        assertThat(graph.V(), is(2));
        assertThat(graph.E(), is(1));
    }
    
    @Test
    public void Parse_Correct_InputLine_many_followers() throws Exception {
        String inputLine = "John follows Jane, Jill";
        Digraph<String> graph = UserFileLineParser.Parse(inputLine);
        
        assertThat(graph.V(), is(3));
        assertThat(graph.E(), is(2));
        assertThat(graph.getEdges("John").get(0), is("Jane"));
        assertThat(graph.getEdges("John").get(1), is("Jill"));
        assertThat(graph.getEdges("Jill").size(), is(0));
        assertThat(graph.contains("Jane"), is(true));
    }
}
