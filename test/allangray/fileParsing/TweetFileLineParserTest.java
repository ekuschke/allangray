
package allangray.fileParsing;

import allangray.Tweet;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class TweetFileLineParserTest {

    @Test(expected = IllegalArgumentException.class)
    public void Line_needs_correct_format() {
        String line = "boo";
        TweetFileLineParser.Parse(line);
    }
    
    @Test
    public void Parse_correct_format() {
        String line = "Alan> If you have a procedure with 10 parameters, you probably missed some.";
        Tweet tweet = TweetFileLineParser.Parse(line);
        
        assertThat(tweet.user, is("Alan"));
        assertThat(tweet.content, is("If you have a procedure with 10 parameters, you probably missed some."));
    }
}
