package allangray.fileParsing;

import allangray.TestHelper;
import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class FileLineReaderTest {
    
    private String fileName = "user.txt";
    
    @After
    public void teardownTest() {
        File file = new File(fileName);
        if(file.exists()) {
            file.delete();
        }
    }

    @Test
    public void ReadLinesFromFile() {
        try {
            String[] lines = new String[] { "Ward follows Alan" };
            TestHelper.createTestFile(lines, fileName);
            assertThat(FileLineReader.readFile(fileName), is(lines));
        } 
        catch (IOException ex) {
            fail(ex.getMessage());
        }
    }
}
