
package allangray;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class TweetTest {
    
    @Test
    public void User_and_content_on_ctor() {
        Tweet tweet = new Tweet("John", "My tweet");
        
        assertThat(tweet.user, is("John"));
        assertThat(tweet.content, is("My tweet"));
    }
    
}
