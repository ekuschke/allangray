
package allangray.collections;

import java.util.ArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class DigraphTest {
    
    public DigraphTest() {
    }
    
    @Test
    public void Add_Vertex_increases_vertices_count() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("John");
        
        Assert.assertThat(graph.V(), is(1));
    }
    
    @Test
    public void Check_if_vertex_present() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("John");    
        
        Assert.assertThat(graph.contains("John"), is(true));
        Assert.assertThat(graph.contains("Sara"), is(false));
    }
    
    @Test
    public void Add_Edge_increases_edges_count() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        
        graph.addEdge("a", "b");
        
        assertThat(graph.E(), is(1));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Add_edge__head_vertex_must_be_present() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        
        graph.addEdge("a", "b");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Add_edge__tail_vertex_must_be_present() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("b");
        
        graph.addEdge("a", "b");
    }
    
    @Test
    public void Remove_edge_decreases_edges_count() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addEdge("a", "b");
        assertThat(graph.E(), is(1));
        
        graph.removeEdge("a", "b");
        assertThat(graph.E(), is(0));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Remove_vertex_not_possible_if_edges_present() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addEdge("a", "b");
        
        graph.removeVertex("a");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Remove_vertex_not_possible_if_head_of_any_edges() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addEdge("a", "b");
        
        graph.removeVertex("b");
    }
    
    @Test
    public void Remove_vertex_with_edge_possible_if_explicitly_specified() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addEdge("a", "b");
        assertThat(graph.V(), is(2));
        assertThat(graph.E(), is(1));
        
        graph.removeVertex("b", true);
        assertThat(graph.E(), is(0));
        assertThat(graph.V(), is(1));
    }
    
    @Test
    public void Get_Adj_Edges_for_vertex() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addEdge("a", "b");
        
        assertThat(graph.getEdges("a").size(), is(1));
        assertThat(graph.getEdges("b").size(), is(0));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void No_duplicate_vertices() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("a");
    }
    
    @Test
    public void Get_Vertices_with_edges_to_vertex() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addVertex("c");
        graph.addEdge("a", "b");
        graph.addEdge("c", "b");
        
        assertThat(graph.getVerticesTo("b").size(), is(2));
    }
    
    @Test
    public void Graph_addition() {
        Digraph<String> graph1 = new Digraph<String>();
        graph1.addVertex("a");
        graph1.addVertex("c");
        graph1.addVertex("d");
        graph1.addEdge("a", "d");
        
        Digraph<String> graph2 = new Digraph<String>();
        graph2.addVertex("b");
        graph2.addVertex("c");
        graph2.addEdge("b", "c");
        
        graph1.combineWith(graph2);
        
        assertThat(graph1.V(), is(4));
        assertThat(graph1.E(), is(2));
    }
    
    @Test
    public void Get_Vertices() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        
        ArrayList<String> vertices = graph.getVertices();
        
        assertThat(vertices.size(), is(2));
        assertTrue(vertices.contains("a"));
        assertTrue(vertices.contains("b"));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void No_duplicate_edges() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addEdge("a", "b");
        graph.addEdge("a", "b");
        
        assertThat(graph.E(), is(1));
    }
    
    public void No_duplicate_edges_created_with_combining_Graphs() {
        Digraph<String> graph = new Digraph<String>();
        graph.addVertex("a");
        graph.addVertex("b");
        graph.addEdge("a", "b");
        
        Digraph<String> graph2 = new Digraph<String>();
        graph2.addVertex("a");
        graph2.addVertex("b");
        graph2.addVertex("c");
        graph2.addEdge("a", "b");
        
        graph.combineWith(graph2);
        
        assertThat(graph.E(), is(1));
        assertThat(graph.V(), is(3));
    }
}
