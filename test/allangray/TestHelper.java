package allangray;

import java.io.FileWriter;
import java.io.IOException;

public class TestHelper {
    
    public static void createTestFile(String[] lines, String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName, false);
        for(String line : lines) {
            writer.write(line);
            writer.write("\n");
        }
        writer.close();
    }
    
}
