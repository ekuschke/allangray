package allangray;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import junit.framework.Assert;
import static org.hamcrest.CoreMatchers.*;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class ProgramTest {
    
    private ByteArrayOutputStream outputBytes;
    private PrintStream console;
    
    private final String INVALID_USERFILE = "Invalid- or no user-file specified.";
    private final String INVALID_TWEETFILE = "Invalid- or no tweet-file specified.";
    
    @Before
    public void setUp() {
        outputBytes = new java.io.ByteArrayOutputStream();
        console = System.out;
        System.setOut(new PrintStream(outputBytes));
    }
    
    @After
    public void tearDown() {
        System.setOut(console);
    }

    @Test
    public void Program_requires_userfile_param() {
        String[] params = new String[] {""};
        Program.main(params);
        
        assertTrue(outputBytes.toString().contains(INVALID_USERFILE));
    }
    
    @Test
    public void Program_requires_tweetfile_param() {
        String[] params = new String[] {""};
        Program.main(params);
        
        assertTrue(outputBytes.toString().contains(INVALID_TWEETFILE));
    }
    
    @Test
    public void Program_accepts_userfile_param() {
        String fileName = "user.txt";
        String[] args = new String[] { "-u", fileName };
        Program.main(args);

        assertFalse(outputBytes.toString().contains(INVALID_USERFILE));
    }
    
    @Test
    public void Program_accepts_tweetfile_param() {
        String fileName = "tweets.txt";
        String[] args = new String[] { "-t", fileName };
        Program.main(args);
        
        assertFalse(outputBytes.toString().contains(INVALID_TWEETFILE));
    }
    
    @Test
    public void Program_outputs_tweets_correctly() throws IOException {
        String userFileName = "user.txt";
        String tweetFileName = "tweets.txt";
        
        String[] userLines = new String[] { 
            "Ward follows Alan",
            "Alan follows Martin",
            "Ward follows Martin, Alan"
        };
        String[] tweetLines = new String[] {
          "Alan> If you have a procedure with 10 parameters, you probably missed some.",
          "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
          "Alan> Random numbers should not be generated with a method chosen at random."
        };
        String expectedResult = "Alan\n" +
            "* @Alan: If you have a procedure with 10 parameters, you probably missed some.\n" +
            "* @Alan: Random numbers should not be generated with a method chosen at random.\n" +
            "\n" +
            "Martin\n" +
            "\n" +
            "Ward\n" +
            "* @Alan: If you have a procedure with 10 parameters, you probably missed some.\n" +
            "* @Ward: There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.\n" +
            "* @Alan: Random numbers should not be generated with a method chosen at random.";
        
        TestHelper.createTestFile(userLines, userFileName);
        TestHelper.createTestFile(tweetLines, tweetFileName);
        
        String[] args = new String[] { "-u", userFileName, "-t", tweetFileName };
        
        Program.main(args);
        
        assertThat(outputBytes.toString(), is(expectedResult));
    }
}
