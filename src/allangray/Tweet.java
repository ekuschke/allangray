
package allangray;

public class Tweet {
    
    public String user = "";
    public String content = "";

    public Tweet(String user, String content) {
        this.user = user;
        this.content = content;
    }
    
}
