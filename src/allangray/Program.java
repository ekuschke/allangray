package allangray;

import allangray.collections.Digraph;
import allangray.fileParsing.FileLineReader;
import allangray.fileParsing.TweetFileLineParser;
import allangray.fileParsing.UserFileLineParser;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;


public class Program {

    public static void main(String[] args) {
        Options options = CreateCommandOptions();
        try {
            CommandLine cmd = new BasicParser().parse(options, args, false);
            
            if(cmd.hasOption("help")) {
                printHelp(options);
                return;
            }
            if(!cmd.hasOption("users")) {
                System.out.println("Invalid- or no user-file specified.");
            }
            if(!cmd.hasOption("tweets")) {
                System.out.println("Invalid- or no tweet-file specified.");
            }
            
            if(cmd.hasOption("users") && cmd.hasOption("tweets")) {
                runCommand(cmd);
            }
        }
        catch(ParseException ex)
        {
            System.out.println("Invalid arguments: " + ex.getMessage());
            printHelp(options);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            printHelp(options);
        }
    }

    private static Options CreateCommandOptions() {
        Options options = new Options();
        options.addOption(OptionBuilder.withLongOpt("users")
                                .withDescription("File containing users")
                                .hasArg()
                                .withArgName("USERFILE")
                                .create("u"));
        options.addOption(OptionBuilder.withLongOpt("tweets")
                                .withDescription("File containing tweets")
                                .hasArg()
                                .withArgName("TWEETFILE")
                                .create("t"));
        options.addOption(new Option("help", "Print this message"));
        return options;
    }
    
    private static void printHelp(Options options) {
        new HelpFormatter().printHelp("allanGray", options);
    }

    private static void runCommand(CommandLine cmd) throws Exception {
        String userFile = cmd.getOptionValue("users");
        String tweetFile = cmd.getOptionValue("tweets");
        
        Digraph<String> userGraph = getUserGraph(readLinesFromFile(userFile));
        ArrayList<Tweet> tweets = getTweets(readLinesFromFile(tweetFile));
        
        outPutTweets(userGraph, tweets);       
    }
    
    private static String[] readLinesFromFile(String fileName) throws FileNotFoundException {
        return FileLineReader.readFile(fileName);
    }
    
    private static Digraph<String> getUserGraph(String[] linesFromFile) throws Exception {
        Digraph<String> result = new Digraph<String>();
        for(String line : linesFromFile) {
            result.combineWith(UserFileLineParser.Parse(line));
        }
        return result;
    }

    private static ArrayList<Tweet> getTweets(String[] linesFromFile) {
        ArrayList<Tweet> result = new ArrayList<Tweet>();
        for(String line : linesFromFile) {
            result.add(TweetFileLineParser.Parse(line));
        }
        return result;
    }
    
    private static void outPutTweets(Digraph<String> userGraph, ArrayList<Tweet> tweets) {
        ArrayList<String> users = userGraph.getVertices();
        Collections.sort(users);
        
        for(String user : users) {
            System.out.println(user);
            
            ArrayList<String> followees = userGraph.getEdges(user);
                
            for(Tweet tweet : tweets) {
                if(followees.contains(tweet.user) || tweet.user.equals(user)) {
                    System.out.println(String.format("* @%s: %s", tweet.user, tweet.content));
                }
            }
            
            System.out.println();
        }
    }
    
}
