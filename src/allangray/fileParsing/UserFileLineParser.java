package allangray.fileParsing;

import allangray.collections.*;

public class UserFileLineParser {

    public static Digraph<String> Parse(String inputLine) throws Exception {
        if(!inputLine.contains(" follows ")) {
            throw new IllegalArgumentException(String.format("Invalid line: %s", inputLine));
        }
        
        String[] seperateBits = inputLine.split(" follows ");
        String tailVertex = seperateBits[0].trim();
        String[] headVertices = seperateBits[1].split(",");
        
        Digraph<String> graph = new Digraph<String>();
        if(!graph.getVertices().contains(tailVertex)) {
            graph.addVertex(tailVertex);
        }
        
        for(String headVertex : headVertices) {
            headVertex = headVertex.trim();
            if(!graph.getVertices().contains(headVertex)) {
                graph.addVertex(headVertex);
            }
            graph.addEdge(tailVertex, headVertex);
        }
        
        return graph;
    }
    
}
