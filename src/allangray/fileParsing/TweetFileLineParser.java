package allangray.fileParsing;

import allangray.Tweet;

public class TweetFileLineParser {
    
    public TweetFileLineParser() {
    }
    
    public static Tweet Parse(String line) {
        if(!line.contains(">")) {
            throw new IllegalArgumentException(String.format("Invalid line: %s", line));
        }
        
        String[] parts = line.split(">");
        
        return new Tweet(parts[0].trim(), parts[1].trim());
    }
    
}
