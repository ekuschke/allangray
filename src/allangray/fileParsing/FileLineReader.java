package allangray.fileParsing;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileLineReader {
    
    public static String[] readFile(String fileName) throws FileNotFoundException {
        List<String> lines = new ArrayList<String>();
        Scanner fileScanner = new Scanner(new File(fileName));
        
        while(fileScanner.hasNextLine()) {
            lines.add(fileScanner.nextLine());
        }
        
        return lines.toArray(new String[lines.size()]);
    }
}
