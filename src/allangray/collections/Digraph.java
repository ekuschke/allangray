
package allangray.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

public class Digraph<T> {
    
    private Hashtable<T, ArrayList<T>> storage;
    private int edgeCount;
    
    public Digraph() {
        this.storage = new Hashtable<T, ArrayList<T>>();
        this.edgeCount = 0;
    }

    public void addVertex(T vertex) {
        if(storage.containsKey(vertex)) {
            throw new IllegalArgumentException("Vertex already exists in graph.");
        }
        storage.put(vertex, new ArrayList<T>());
    }
    
    public int V() {
        return storage.size();
    }
    
    public int E() {
        return edgeCount;
    }
    
    public boolean contains(T vertex) {
        return storage.containsKey(vertex);
    }

    public void addEdge(T tailVertex, T headVertex) {
        if(!storage.containsKey(headVertex)) {
            throw new IllegalArgumentException("Specified head vertex does not exist in graph.");
        }
        if(!storage.containsKey(tailVertex)) {
            throw new IllegalArgumentException("Specified tail vertex does not exist in graph.");
        }
        if(storage.get(tailVertex).contains(headVertex)) {
            throw new IllegalArgumentException("Specified edge already exists in graph.");
        }
        storage.get(tailVertex).add(headVertex);
        edgeCount++;
    }

    public void removeEdge(T tailVertex, T headVertex) {
        storage.get(tailVertex).remove(headVertex);
        edgeCount--;
    }

    public void removeVertex(T vertex) {
        if(storage.get(vertex).size() != 0) {
            throw new IllegalArgumentException("Specified vertex has adjacent edges.");
        }
        for(T tailVertex : storage.keySet()) {
            if(storage.get(tailVertex).contains(vertex)) {
                throw new IllegalArgumentException("Specified vertex is head of one or more edge(s).");
            }
        }
        storage.remove(vertex);
    }
    
    public void removeVertex(T vertex, boolean removeAdjacentEdges) {
        if(removeAdjacentEdges) {
            for(T tailVertex : storage.keySet()) {
                if(storage.get(tailVertex).contains(vertex)) {
                    removeEdge(tailVertex, vertex);
                }
            }
            if(storage.containsKey(vertex)) {
                storage.remove(vertex);
            }
        } else {
            removeVertex(vertex);
        }
    }
    
    public ArrayList<T> getEdges(T tailVertex) {
        return storage.get(tailVertex);
    }

    public ArrayList<T> getVerticesTo(T headVertex) {
        ArrayList<T> result = new ArrayList<T>();
        for(T tailVertex : storage.keySet()) {
            if(storage.get(tailVertex).contains(headVertex)) {
                result.add(tailVertex);
            }
        }
        return result;
    }

    public void combineWith(Digraph<T> graph2) {
        for(T tailVertex : graph2.getVertices()) {
            if(!storage.containsKey(tailVertex)) {
                addVertex(tailVertex);
            }
            for(T headVertex : graph2.getEdges(tailVertex)) {
                if(!this.getEdges(tailVertex).contains(headVertex)) {
                    if(!this.storage.containsKey(headVertex)) {
                        this.addVertex(headVertex);
                    }
                    this.addEdge(tailVertex, headVertex);
                }
            }
        }
    }

    public ArrayList<T> getVertices() {
        return new ArrayList<T>(storage.keySet());
    }
}
